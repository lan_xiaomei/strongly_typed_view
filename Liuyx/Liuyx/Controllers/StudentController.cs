﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Liuyx.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StudentMessage()
        {
            List<Student> students = new List<Student>();

            for (int i = 0; i < 20; i++)
            {
                var stu = new Student { Name = String.Format("学生{0}", i), Age = 20 };
                students.Add(stu);
            }
            ViewBag.List = students;
            return View();
        }
        public class Student
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }
    }
}