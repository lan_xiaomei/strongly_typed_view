﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.PageTitle = "学生管理";
            return View();
        }

        public ActionResult List()
        {
            List<Student> StudentList = new List<Student>();

            for (int i = 0; i < 5; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}", i), Age = 18 };
                StudentList.Add(stu);
            }

            ViewBag.List = StudentList;


            return View();
        }

        

    }
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}