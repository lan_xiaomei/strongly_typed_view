﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_2.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.TitlePage = "学生信息管理中心";
            return View();
        }

        public ActionResult List()
        {
            return View("~/Views/Home/Index.cshtml");
        }

        public ActionResult Listting()
        {
            List<Student> students = new List<Student>();
            for (int i = 0;i < 10; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}",i),Age=6*i,Address = string.Format("学生所在学校:闽西职业技术学院") };
                students.Add(stu);
            }
            //ViewBag.Lists = students;


            return View(students);
        }

       
    }
     public class Student
        {
            public string Name { get; set; } 
            public int Age { get; set; }
            public string Address { get; set; }
        }
}