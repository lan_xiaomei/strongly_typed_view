﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "花园管理界面。";
            return View();
        }

        //public ActionResult List()
        //{
        //    return View("~/Views/Home/Pget.cshtml");
        //}

        public ActionResult List()
        {
            List<Student> studentList = new List<Student>();

            for (int i = 0; i < 5; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}", i), Age =10 };
                studentList.Add(stu);
            }

            ViewBag.List = studentList;

            return View();
        }
    }
    public class Student
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}