﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.stuName = "qwer";
            return View();
        }
        public ActionResult List()
        {

            return View("~/Views/Home/About.cshtml");
        }
        public ActionResult Test() 
        {
            List<Students> stuList = new List<Students>();
            for (var i = 1; i<=6; i++)
            {
                var stu = new Students { Name = string.Format("学生编号{0}", i), Age = 18 };
                stuList.Add(stu);
            }
            ViewBag.Stuinfo = stuList;
            return View();
        }
       
    }
    public class Students 
    {
    

        public string Name { get; set; }
        public int Age { get; set; }


    }
        
}