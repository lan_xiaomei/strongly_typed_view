﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "就看看";
            return View();
        }

        public ActionResult About()
        {

            ViewBag.Name = "李育奇";
            ViewBag.Message = "mvc.";

            return View("Index");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "联系方式.";

            return View();
        }
    }
}