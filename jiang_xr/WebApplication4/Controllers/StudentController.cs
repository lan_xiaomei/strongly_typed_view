﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication4.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Qiang()
        {

            List<Student> students = new List<Student>();

            for (int i=0;i<5;i++)
            {
                var stu = new Student { Name = string.Format("学生{0}", i), Age = 100 };
                students.Add(stu);
            }

            ViewBag.List = students;

            return View();
        }

    }

    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}