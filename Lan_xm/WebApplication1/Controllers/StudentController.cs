﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            //viewBag后面可以随意取名字
            ViewBag.PageTitle = "学生有关增删改查";
            return View();
        }

        public ActionResult List()
        {

            //return View("Index");
            // 第五种可视化方式：直接输入路径
            //return View("~/Views/Home/Index.cshtml");

            List<Student> studentList = new List<Student>();
            for(int i = 0; i < 5; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}",i),Class=100 };
                studentList.Add(stu);
            }

            ViewBag.List = studentList;

            return View();
        }

    }
    public class Student
    {
        public string Name { get; set; }

        public int Class { get; set; }
    }

}