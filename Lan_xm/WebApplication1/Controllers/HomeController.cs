﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            //第一种可视化方式：显示页面的title
            ViewBag.Title = "关于界面";

            //第二种可视化方式：在这里写内容，传入Index.cshtml
            ViewBag.Rbac = "某人";
            

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        //第三种可视化方式：新建一个视图
        public ActionResult MouMou()
        {
            //第四种可视化方式：在指定的视图文件夹寻找
            return View("About");
        }
    }
}