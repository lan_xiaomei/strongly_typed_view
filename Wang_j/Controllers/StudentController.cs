﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student

        public ActionResult Listing()
        {
            var listing = new List<Student>();

            for (int i = 0; i < 10; i++)
            {
                listing.Add(new Student { Name = "学生" + i.ToString() });
            }

            //ViewBag.Listing = listing;
            
            return View();
        }
    }

}