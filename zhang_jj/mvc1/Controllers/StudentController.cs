﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvc1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.Title = "学生管理";
            return View();
        }

        public ActionResult God()
        {
            return View("~/Views/Home/About.cshtml");
        }

        public ActionResult List()
        {
            List<Student> students = new List<Student>();

            for (int i = 0; i < 5; i++)
            {
                var str = new Student { name = string.Format("学生{0}", i), age = 100 };
                students.Add(str);
            }

            ViewBag.list = students;
            return View();
        }
        

    }
    public class Student
    {
        public string name { get; set; }
        public int age { get; set; }
    }
}