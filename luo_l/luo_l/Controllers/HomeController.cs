﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace luo_l.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "泰罗奥特曼打怪兽";
            ViewBag.Message = "Your application description page.";
            ViewBag.Ast = "我很喜欢一个女孩子";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult yang()
        {
            return View("About");
        }

    }
}