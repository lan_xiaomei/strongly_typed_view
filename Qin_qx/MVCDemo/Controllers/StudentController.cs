﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {

            ViewBag.Title = "学生管理";

            return View();
        }

        public ActionResult List()
        {
            List<Student> studentList = new List<Student>();

            for (int i = 0; i < 5; i++)
            {
                var stu = new Student { Name = string.Format("学生{0}号", i) };
                studentList.Add(stu);
            }

            ViewBag.List = studentList;

            return View();
        }

        
    }
    public class Student
    {
        public string Name { get; set; }
    }
}